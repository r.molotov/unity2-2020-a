﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Photon.Pun;
using UnityStandardAssets.Characters.FirstPerson;
using Photon.Compression;

public class PlayerController : MonoBehaviourPunCallbacks
{
    InteractiveItem interactiveItem;
    RaycastHit hit;
    Camera cam;
    Weapon weapon;

    [SyncVar] public float health;
    void Start()
    {
        if (photonView.IsMine)
        {
            GameManager.Instance().localPlayer = this;

            GameManager.Instance().HUD.enabled = true;

            cam = GetComponentInChildren<Camera>();
            weapon = GetComponentInChildren<Weapon>();

            cam.enabled = true;
            weapon.enabled = true;
            GetComponentInChildren<AudioListener>().enabled = true;

            GetComponent<CharacterController>().enabled = true;
            GetComponent<FirstPersonController>().enabled = true;
        }
    }

    void Update()
    {
        if (!photonView.IsMine) return;

        Physics.Raycast(
            cam.transform.position + cam.transform.forward,
            cam.transform.forward,
            out hit,
            3f);
        print(hit.collider?.gameObject.name);
        interactiveItem = hit.collider?
            .GetComponent<InteractiveItem>();

        if (interactiveItem)
            GameManager.Instance().hintText.text = "Press E to use " +
                interactiveItem.ItemName;
        else
            GameManager.Instance().hintText.text = "";

        if (Input.GetMouseButtonDown(0))
        {
            weapon.Attack();
        }
        if (Input.GetKeyDown(KeyCode.E))
        {

            interactiveItem?.Use();
        }
    }
}
